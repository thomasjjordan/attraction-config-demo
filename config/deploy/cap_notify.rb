require "action_mailer"

ActionMailer::Base.delivery_method       = :smtp
ActionMailer::Base.raise_delivery_errors = true
ActionMailer::Base.smtp_settings         = {
    :address             => "smtp.hudsonltd.com",
    :port                => 25,
    :user_name           => 'update@hudsonltd.com',
    :password            => 'railzdeploy85',
    :authentication      => :login,
    :openssl_verify_mode => 'none'
}


class Notifier < ActionMailer::Base
  default :from => "update@hudsonltd.com"
  def deploy_notification(cap_vars)
    msg = "#{cap_vars.message}"

    mail(:to      => cap_vars.notify_emails,
         :subject => "*** Version Removal ***") do |format|
      format.text { render :text => msg}
      format.html { render :text => "<p>" + msg + "<\p>" }
    end
  end
end
