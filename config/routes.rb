Rails.application.routes.draw do
  scope ENV['RAILS_RELATIVE_URL_ROOT'] || '/' do
    mount HudsonBase::Engine, :at  => 'hudson_base', :as => 'hudson_base'

		get ':site_id/:id' => 'location#index', as: :detailed_location
    post ':site_id/:id' => 'location#save', as: :update_location
  end
end
