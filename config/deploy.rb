# Deploy script as adapted from the book "Deploying Rails"

# Deploying to production:
#> cap deploy

# Deploying to demo:
#> cap demo deploy

# Setting up a new app version:
#> cap new_version:setup
#-------------

# ----- Configuration you may need to change:

# If you change the name or location of this app:

# Setup the emails, and after deploy hook

set :base_app, 'location_config' # The base pathname for this app on the server.

set :app_version, '1.0'

# If you change which ruby this app uses:
#   change the PassengerRuby line in the :update_htaccess task below.
#   and change this line:
set :rvm_ruby_string, '2.1.1'


# -----
# ----- You shouldn't have to change anything below this line!
# -----

require 'net/http'
require 'bundler/capistrano'
require 'rvm/capistrano'
require 'capistrano_colors'
load 'config/deploy/cap_notify'
require 'socket'

set :application, "#{base_app}#{app_version}"
set :scm, :git

# Release environment config (demo, production)
role(:web) { Net::HTTP.get('web10.hudsonltd.net', '/rails/host_list').split("\n") }
set :stages, %w(demo production)
set :default_stage, 'production'
require 'capistrano/ext/multistage'

# Deployment config
set :user, 'perladm'
set :group, 'perladm'
set :rvm_type, :system
set :deploy_to, "/var/www/rails_apps/#{application}/prod"
set :use_sudo, false
set :deploy_via, :remote_cache
set :copy_strategy, :export
set :keep_releases, 3
set :email_app_name, :application
t = Time.now
t2 =  t - 4*60*60
set :time_of_execution, t2.strftime("%A %b %d %Y %I:%M %p")
set :user_ip, Socket::getaddrinfo(Socket.gethostname, "echo", Socket::AF_INET)[0][3]
set :release_type, 'Production'
set :notify_emails, ["rails-dev@hudsonltd.com"]
after "delete_version:remove", 'email:send_notification'


# Figure out deploy branch.
current_branch = `git branch`.match(/\* (\S+)\s/m)[1]
set :branch, ENV['branch'] || current_branch || "master"
if current_branch != 'master'
  puts <<-WARN

  ========================================================================

    WARNING: You're deploying from the
       << #{current_branch} >> branch
    instead of master.

    That's fine if you intended to, just making sure.

  ========================================================================

  WARN
  answer = Capistrano::CLI.ui.ask "  Are you sure you want to continue? (Y to continue) "
  exit if answer.upcase != 'Y'
end

namespace :deploy do
  task :start do
    ;
  end
  task :stop do
    ;
  end

  desc "Restart the application"
  task :restart, :roles => :web, :except => {:no_release => true} do
    run "#{try_sudo} touch #{File.join(current_path, 'tmp', 'restart.txt')}"
  end
  after "deploy:restart", "deploy:cleanup"

  desc "Clear the remote cache"
  task :clear_remote_cache, :roles => [:web] do
    run "rm -rf #{shared_path}/cached-copy"
    run "rm -rf /var/www/rails_apps/#{application}/dvl/shared/cached-copy"
  end

  desc "Copy the database.yml file into the latest release"
  task :copy_in_database_yml do
    run "cp #{latest_release}/config/database.#{base_app}.yml #{latest_release}/config/database.yml"
    # This will ultimately be how this task is executed.
    # run "cp #{shared_path}/config/database.yml #{latest_release}/config/"
  end

  desc "Create a .htaccess file for Phusion Passenger."
  task :update_htaccess do
    template = <<-EOF
PassengerEnabled on
RailsEnv #{rails_env}
SetEnv RAILS_RELATIVE_URL_ROOT /a/r/#{application}
PassengerRuby /usr/local/rvm/wrappers/ruby-2.1.1/ruby
PassengerAppRoot #{deploy_to}/current

PassengerMinInstances 1
    EOF
    put template, "#{release_path}/public/.htaccess"
  end
  before "deploy:restart", "deploy:update_htaccess"
end
before "deploy:assets:precompile", "deploy:copy_in_database_yml"

# Tasks for creating a new application version.
# Be sure to update the :app_version above before running.
namespace :new_version do
  desc "Setup a new application version on the servers."
  task :setup do
    create_phusion_symlinks
    system("cap deploy:setup")
    system("cap demo deploy:setup")
  end

  task :create_phusion_symlinks do
    # Ensure the base application /a/<app> exists.
    run "ln -sf /var/www/rails_apps/redirector/prod/current/public /var/www/html/a/#{base_app}"
    run "ln -sf /var/www/rails_apps/redirector/prod/current/public /var/www/html/a/#{base_app}_demo"

    # Create the /a/r/<application> links for prod and demo.
    run "ln -sf /var/www/rails_apps/#{application}/prod/current/public /var/www/html/a/r/#{application}"
    run "ln -sf /var/www/rails_apps/#{application}/dvl/current/public /var/www/html/a/r/#{application}_demo"
  end
end

namespace :delete_version do
  set(:old_version, Capistrano::CLI.ui.ask('Enter the version you wish to remove:'))
  desc "Remove old and outdated version off of the server"
  task :remove do


    answer = Capistrano::CLI.ui.ask "========================================================================

    WARNING: You're about to delete

        #{fetch(:old_version)}

    Are you sure this is the version you intended to delete?

    ========================================================================= \n\nAre you sure you want to continue? (Y to continue) "
    exit if answer.upcase != 'Y'
    # Not the best looking and cleanest solution. Capistrano 3.x has a much better way to do this, but it looks like
    # updating Capistrano will be pretty work intensive. (03-30-2015)
    run "if test -d /var/www/rails_apps/#{fetch(:old_version)};
          then
          echo '';
          echo '-----------------------------------------------------------';
          echo '#{fetch(:old_version)} is being removed from $CAPISTRANO:HOST$';
          echo '-----------------------------------------------------------';
          echo '';
          rm /var/www/html/a/r/#{fetch(:old_version)};
          mkdir /var/www/html/a/r/#{fetch(:old_version)};
          echo 'Redirect 301 /a/r/#{fetch(:old_version)} /a/#{fetch(:base_app)}' > /var/www/html/a/r/#{fetch(:old_version)}/.htaccess;
          rm /var/www/html/a/r/#{fetch(:old_version)}_demo;
          rm -rf /var/www/rails_apps/#{fetch(:old_version)};
        else
          echo '';
          echo '-----------------------------------------------------------';
          echo '#{fetch(:old_version)} does not exist on $CAPISTRANO:HOST$';
          echo '------------------------------------------------------------';
          echo '';
        fi"

    # Create/update activity.log in /var/www/rails_apps/shared/ directory, every time task is run.

    run " echo 'Capistrano delete_version:remove task' >> /var/www/rails_apps/shared/activity.log;
          echo '[* * SERVER: $CAPISTRANO:HOST$ * *]  IP ADDRESS: #{fetch(:user_ip)} , VERSION TARGETED: #{fetch(:base_app)} v.#{fetch(:old_version)}, EXECUTED ON: #{fetch(:time_of_execution)}' >> /var/www/rails_apps/shared/activity.log;
          echo '' >> /var/www/rails_apps/shared/activity.log;"
  end
end
set :message, (capture "echo '[* * SERVER: $CAPISTRANO:HOST$ * *]';
                        echo 'IP Address: #{fetch(:user_ip).to_s}';
                        echo 'Version Targeted: #{fetch(:base_app)} v.#{fetch(:old_version)}';
                        echo 'Executed On: #{fetch(:time_of_execution)}'")

namespace :email do
  desc "Send email notification"
  task :send_notification do
    Notifier.deploy_notification(self).deliver_now
  end
end
