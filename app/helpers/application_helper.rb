require 'paleta'
module ApplicationHelper

  def notice_box
    notice_class = 'alert'
    notice_class << ' alert-danger' if flash[:error]
    notice_class << ' alert-info' if flash.notice
    notice_class << ' hide' unless flash[:error] || flash.notice

    content_tag(:div, id: 'notice_box', class: notice_class) do
      (flash[:error] || flash.notice).try(:html_safe)
    end.html_safe
  end

  def subsection_tab_color
    color = Paleta::Color.new(:hex, current_theme.subsection_background_color.gsub(/#/, ''))
    "#" + "#{color.darken!(5).hex}"
  end

  def display_attr_tab?
    display_tab_token = Tokens.for(current_site_id, 'LOCCFG_DISPLAY_ATTR_TAB')
    display_tab_token.blank? ? false : true
  end
end
