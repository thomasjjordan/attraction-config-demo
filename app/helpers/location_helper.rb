
module LocationHelper
  def error_class(location,field)
    (flash[:error_fields] && flash[:error_fields].include?(field)) ?
        'has-error error-field has-feedback' : ''
  end

end