function previewImage() {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

    oFReader.onload = function (oFREvent) {
        document.getElementById("uploadPreview").src = oFREvent.target.result;
    };
}

function previewThumbImage() {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("uploadThumbImage").files[0]);

    oFReader.onload = function (oFREvent) {
        document.getElementById("uploadThumbPreview").src = oFREvent.target.result;
    };

}

function exchangeThumbImage() {
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("uploadThumbImage").files[0]);

  oFReader.onload = function (oFREvent) {
    document.getElementById("exchangeThumbPreview").src = oFREvent.target.result;
  };

}

function exchangeImage() {
  var oFReader = new FileReader();
  oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

  oFReader.onload = function (oFREvent) {
    document.getElementById("exchangeImagePreview").src = oFREvent.target.result;
  };

}