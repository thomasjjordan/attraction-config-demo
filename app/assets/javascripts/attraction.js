window.onload = function () {

  //<-----Full Size Image Handling----->//

  $('#close-image-modal, #image-cancel').click(clearImage);

  function clearImage() {
    $(":file").filestyle('clear');
    $("#uploadPreview").attr("src", "")
  }

  $('#image-upload-button').click(imageModalToggle);

  function imageModalToggle() {
    $('#imageModal').modal('toggle')
  }

  $('#uploadImage').change(previewImage);

  $("#image-save").click(function () {
    exchangeImage();
    $('.image-download').css('display', 'block');
    $('#exchangeImagePreview').css('display', 'block');
    $('#image-preview-close').css('display', 'block');
    $('#image-preview-container').css('display', 'inline-block');
  });

  $("#image-cancel, #close-image-modal, #image-preview-close").click(function () {
    $('.image-download').css('display', 'none');
    $('#exchangeImagePreview').css('display', 'none');
    $('#image-preview-close').css('display', 'none');
    $('#image-preview-container').css('display', 'none');
    clearImage()
  });

  //<-----Thumb Image Handling----->//

  $('#close-thumb-modal, #thumb-image-cancel ').click(clearThumbImage);

  function clearThumbImage() {
    $(":file").filestyle('clear');
    $("#uploadThumbPreview").attr("src", "")
  }

  $('#image-thumb-upload-button').click(imageThumbModalToggle);

  function imageThumbModalToggle() {
    $('#imageThumbModal').modal('toggle')
  }

  $('#uploadThumbImage').change(previewThumbImage);

  $("#thumb-image-save").click(function () {
    exchangeThumbImage();
    $('#exchangeThumbPreview').css('display', 'block');
    $('.thumb-download').css('display', 'block');
    $('#thumb-preview-close').css('display', 'block');
    $('#thumb-preview-container').css('display', 'inline-block');
  });

  $("#thumb-image-cancel, #close-thumb-modal, #thumb-preview-close").click(function () {
    $('.thumb-download').css('display', 'none');
    $('#exchangeThumbPreview').css('display', 'none');
    $('#thumb-preview-close').css('display', 'none');
    $('#thumb-preview-container').css('display', 'none');
    clearThumbImage()
  });

  $('[data-toggle="popover"]').popover({
    trigger: 'hover',
    'placement': 'top'
  });

};










