class LocationController < ApplicationController
  display_tokens 'LOCCFG_'         => {
                     loc_tab_label:             {'TAB_LOC' => 'Location Details'},
                     attr_tab_label:            {'TAB_ATTR' => 'Attraction Details'},
                     attr_pic_tab_label:        {'TAB_ATTR_PIC' => 'Attraction Pictures'},
                     extend_tab_label:          {'TAB_EXTENDED' => 'Extended Details'},
                     loc_tab_icon:              {'LOCATION_TAB_ICON' => 'globe'},
                     attr_tab_icon:             {'ATTRACTION_TAB_ICON' => 'map-marker'},
                     attr_pic_tab_icon:         {'ATTRACTION_PHOTO_TAB_ICON' => 'camera'},
                     extend_tab_icon:           {'EXTENDED_TAB_ICON' => 'info-circle'},
                     image_upload_header:       {'IMAGE_UPLOAD_HEADER' => 'Upload Full Size Image'},
                     image_thumb_upload_header: {'IMAGE_THUMB_UPLOAD_HEADER' => 'Upload Image Thumb'},
                     image_upload_save:         {'IMAGE_UPLOAD_SAVE' => 'Save'},
                     image_upload_cancel:       {'IMAGE_UPLOAD_CANCEL' => 'Cancel'},
                     location_save_message:     {'CAP_SAVE_MESSAGE' => 'Your location details have been saved.'}

                 },
                 'LOCCFG_LOC_'     => {
                     country_label:   {'' => 'Country'},
                     location_label:  {'' => 'Location'},
                     fare_name_label: {'' => 'Fare Name'},
                     fare_code_label: {'' => 'Fare Code'},
                     fare_type_label: {'' => 'Fare Type'},
                     fare_opts_label: {'' => 'Fare Option'}
                 },

                 'LOCCFG_ATTR_'    => {
                     summary_descr_label:  'SUMMARY_DESCR',
                     special_info_label:   'SPECIAL_INFO',
                     instructions_label:   'INSTRUCTIONS',
                     details_label:        'DETAILS',
                     terms_and_cond_label: 'TERMS_CONDITIONS',
                     thumb_upload_header:  {'THUMB_UPLOAD_HEADER' => 'Thumbnail Image'},
                     image_upload_header:  {'IMAGE_UPLOAD_HEADER' => 'Full Size Image'},
                     thumb_upload_button:  {'CAP_THUMB_UPLOAD_BUTTON' => 'Upload Thumb Image'},
                     image_upload_button:  {'CAP_IMAGE_UPLOAD_BUTTON' => 'Upload Image'},
                     thumb_upload_descr:   {'THUMB_UPLOAD_BUTTON_DESCR' => 'This Image will be displayed as a Thumbnail Image'},
                     image_upload_descr:   {'IMAGE_UPLOAD_BUTTON_DESCR' => 'This Image will be displayed as a Full Sized Image'}
                 },
                 'LOCCFG_ATTR_PIC' => {
                     attr_photo_label: {'' => 'Attraction Photos'}
                 },
                 'LOCCFG_EXTEND_'  => {
                     source_name_label:   'SOURCE_NAME',
                     source_TOD_label:    'SOURCE_TOD',
                     last_update_label:   'LAST_UPDATE_NAME',
                     record_update_label: 'RECORD_UPDATE_TOD',
                 }
  tokens 'LOCCFG_'           => {

         },

         raw_address_label:  {'ROADDRESSDESC' => 'Address'},
         raw_address2_label: {'ADDRESSDESC2' => 'Address2'},
         raw_city_label:     {'CITYDESC' => 'City'},
         raw_state_label:    {'STATEDESC' => 'State'},
         raw_zip_label:      {'PICKUPZIPDESC' => 'Zip'}


  def index
    @detailed_location = DetailedLocation.find(@site_id, params[:id])
  end

  def save
    @detailed_location = DetailedLocation.find(current_site_id, params[:id])
    @detailed_location.save(current_site_id, params[:detailed_location].merge(ID: params[:id]))

    if @detailed_location.save_errors?
      redirect_to detailed_location_path
      flash[:save_message] = token('LOCCFG_CAP_SAVE_MESSAGE').html_safe
    else
      flash.now[:error_fields] = @detailed_location.error_fields
      flash.now[:error]        = @detailed_location.save_errors
      render :action => 'index'
    end
  rescue StandardError => e
    send_exception_report(e)
    redirect_to :back, flash: {error: ['We were unable to update your location']}
  end
end