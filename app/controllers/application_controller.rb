class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include HasSite
  include HasDisplayTokens

  before_action :check_for_bad_site
  before_action :set_time_zone

  display_tokens 'LOCCFG_' => {
                     application_title:  {'APP_TITLE' => 'Location Configuration'},
                       submit_button_text: {'SUBMIT_BUTTON_TEXT' => 'Submit'},

                 }

  unless Rails.env.development?
    rescue_from Exception, :with => :render_500
    rescue_from ActiveRecord::RecordNotFound, ActionController::RoutingError, ActionController::UnknownController, ::AbstractController::ActionNotFound, :with => :render_404
  end

  def check_for_bad_site
    SiteServer.for(current_site_id) if current_site_id
  rescue HudsonWebService::ServiceError => e
    if e.to_s =~ /Site not found/
      logger.error "Unable to find site for <#{current_site_id}>, displaying error."
      render text: 'Site not found'
    end
  end

  def current_theme
    @theme ||= Layout::Colors.new(current_site_id, current_group_id)
  end

  helper_method :current_theme

  def send_exception_report(exception)
    Alarm.new(current_site_id, exception, request.env).ring
  end

  def set_time_zone
    Time.zone = current_site.time_zone if current_site_id
  end
end